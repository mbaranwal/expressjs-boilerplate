const { Response, Logger } = require('../utilities');
const userRoutes = require('./modules/user/userRoute');

exports.loadRoutes = (app, prefix) => {
  app.use(`${prefix}/users`, userRoutes);

  app.get('/status', (req, res) => {
    Logger.log('info', 'checking status', { status: 1 });

    Response.success(res);
  });
};
