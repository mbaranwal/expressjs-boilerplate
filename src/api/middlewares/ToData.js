module.exports = (req, res, next) => {
  if (req.method === 'GET') {
    req.data = { ...req.query, ...req.params };
  } else {
    req.data = { ...req.body, ...req.params };
  }

  next();
};
