const Multer = require('./Multer');
const ToData = require('./ToData');

module.exports = {
  Multer,
  ToData,
};
