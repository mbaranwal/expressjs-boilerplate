const { Response, Logger } = require('../../../utilities');
const { UserService } = require('../../../services');

class UserController {
  static async getAll(req, res) {
    try {
      Logger.log('info', 'Fetching users');

      const srvRes = await UserService.getAll();

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async getOne(req, res) {
    try {
      Logger.log('info', 'Fetching users');

      const srvRes = await UserService.getOne(req.data);

      Response.success(res, srvRes);
    } catch (e) {
      Response.fail(res, e);
    }
  }

  static async upload(req, res) {
    Logger.log('info', 'sample file upload api');

    Response.success(res, 'success', req.data);
  }
}

module.exports = UserController;
