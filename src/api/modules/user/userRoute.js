const { Router } = require('express');

const { Multer, ToData } = require('../../middlewares');

const controller = require('./userController');
const validation = require('./userValidation');

const router = Router();

router.get('', ToData, controller.getAll);
router.get('/:userId', validation.getOne, ToData, controller.getOne);

router.post('/upload', Multer.single('file'), validation.upload, ToData, controller.upload);

module.exports = router;
