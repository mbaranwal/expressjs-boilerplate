const dotenv = require('dotenv');

/* eslint-disable global-require */
(async () => {
  try {
    // loading env file
    const envPath = `${__dirname}/../../.env`;
    dotenv.config({
      path: envPath,
    });

    // Loading main package
    await require('../app');
  } catch (e) {
    // Deal with the fact the chain failed
    // eslint-disable-next-line no-console
    console.log('error in loading env', e);
  }
})();
