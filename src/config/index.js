const config = {
  IS_LOCAL: process.env.NODE_ENV === 'local',
  IS_PROD: process.env.NODE_ENV === 'prod',

  port: parseInt(process.env.PORT, 10) || 3000,

  database: {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    name: process.env.DB_NAME,
    host: process.env.DB_HOSTNAME,
    dialect: 'mysql',
    charset: 'utf8',
    collate: 'utf8mb4_unicode_ci',
  },

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || 'info',
  },
};

module.exports = config;
