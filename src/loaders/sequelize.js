const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const Config = require('../config');

const basename = path.basename(__filename);
const Models = {};

const modelsPath = `${__dirname}/../models`;

Sequelize.Promise.config({
  longStackTraces: true,
});

const sequelize = new Sequelize(Config.database.name, Config.database.user, Config.database.password, {
  host: Config.database.host,
  dialect: Config.database.dialect,
  charset: Config.database.charset,
  collate: Config.database.collate,
});

fs.readdirSync(modelsPath)
  .filter(file => file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js')
  .forEach(file => {
    const model = sequelize.import(path.join(modelsPath, file));
    Models[model.name] = model;
  });

Object.keys(Models).forEach(modelName => {
  if (Models[modelName].associate) {
    Models[modelName].associate(Models);
  }
});

module.exports = {
  sequelize,
  Models,
};
