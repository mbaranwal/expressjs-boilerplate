module.exports = (queryInterface, DataTypes) => {
  const User = queryInterface.define(
    'user',
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
      },
      createdById: {
        type: DataTypes.INTEGER,
      },
      updatedById: {
        type: DataTypes.INTEGER,
      },
    },
    {
      timestamp: true,
      paranoid: true,
      defaultScope: {
        attributes: { exclude: ['createdById', 'updatedById', 'deletedAt'] },
      },
    },
  );

  User.associate = models => {
    // User.belongsTo(models.author);
  };

  return User;
};
