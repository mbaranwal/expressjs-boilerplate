const { Response, Logger, Message } = require('../utilities');
const { Models } = require('../loaders/sequelize');

class UserService {
  static async getAll() {
    try {
      Logger.log('info', 'Fetching users');

      const users = await Models.user.findAll();

      return {
        data: users,
      };
    } catch (e) {
      Logger.log('error', 'Fetching users', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }

  static async getOne(params) {
    try {
      Logger.log('info', 'Fetching user');

      const users = await Models.user.findOne({
        id: params.userId,
      });

      return {
        data: users,
      };
    } catch (e) {
      Logger.log('error', 'Fetching user', e);

      throw Response.createError(Message.tryAgain, e);
    }
  }
}

module.exports = UserService;
